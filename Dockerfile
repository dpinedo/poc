FROM ubuntu:18.04

# Install required packages
RUN apt-get update -yqq
# Install required packages
RUN apt-get install -yqq --no-install-recommends \
        build-essential \
        ninja-build \
        pkg-config \
        libglib2.0-dev \
        python3 \
        python3-setuptools \
        python3-pip \
        python3-wheel \
        clang-tools-9 \
        gcovr  \
        lcov \
        valgrind

RUN pip3 install scan-build meson
RUN export PATH="$PWD/tests:$PATH"
